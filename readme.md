### Instalation 
Here's step for install Moxie WP Test plugin :  

- Download the plugin from https://bitbucket.org/didiroesmana/moxie-wp-test/downloads .
- Unzip it into the WordPress plugins folder (wp-dir/wp-content/plugins).
- Or you can upload it via Plugins -> Add New -> Upload File .
- Activate the plugin .
- Don't forget to install Plugin Depedencies .
- Add some movies from Backend .
- Voila !!! Now you can see Movies list on Frontpage .

### To Do 
- Caching the API
- PHP Unit tests
- Travis CI
