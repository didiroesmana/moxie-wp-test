jQuery( document ).ready(function() {
	jQuery.ajax({
		url: "wp-admin/admin-ajax.php",
		data: {
			action: "get_movies_list",
		}
	})
	.always( function( response ){
		setTimeout(function() {
			if ( response['data'] != '' ) {
				for ( var i=0; i < response['data'].length ; i++ ){
					var movie = 
					'<li class="moxie-movie-item" id="movie-'+ response['data'][i].id +'">' +
							'<div class="moxie-movie-image"><img src="'+ response['data'][i].poster_url +'">' +
							'</div>' +
							'<div class="moxie-movie-header">'+
								'<div class="moxie-movie-title-header">'+ response['data'][i].title +
									'<div class="moxie-movie-year-header"> ( '+ response['data'][i].year +
									' ) </div>'+
								'</div>'+
							'</div>'+
							'<div class="moxie-movie-rating"> Rating : '+ response['data'][i].rating + ' from 5'+
							'</div>'+
							'<div class="moxie-movie-description">' + response['data'][i].short_description +
							'</div>'+
					'</li>';
					
					jQuery("#moxie-movie-slider").append(movie);

				}
				jQuery("#moxie-movie-slider").bxSlider({
				    auto: true,
				    pause: 4000,
				    mode: 'fade',
				    speed: 1500,
				    easing: 'ease-in'
				});
				jQuery("div#moxie-movie-loading").hide();
			} else {
				jQuery("div#moxie-movie-loading").hide();
				jQuery("div#moxie-no-movie").show();
			}
		}, 2000 );
	});

});