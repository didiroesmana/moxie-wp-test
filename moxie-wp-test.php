<?php
/*
Plugin Name: Moxie WP Test
Plugin URI: https://bitbucket.org/didiroesmana/moxie-wp-test/
Description: A plugin that displays a list of movies in frontpage.
Version: 0.1
Author: didiroesmana
Author URI: https://bitbucket.org/didiroesmana/
*/
/**
 * Prevent direct access
 */
defined( 'ABSPATH' ) or die( 'Plugin file cannot be accessed directly.' );


/**
 * Include TGMPA
 */
require_once dirname( __FILE__ ) . '/resource/class-tgm-plugin-activation.php';

if ( ! class_exists( 'MoxieWpTestPlugin' ) ) {
	class MoxieWpTestPlugin
	{
		public function __construct()
		{
			/**
			 * Register plugin depedencies
			 * Remove ACF menu from admin
			 */
			add_action( 'tgmpa_register', array( &$this, 'install_required_plugins' ) );
			add_action( 'admin_menu', array( &$this, 'remove_acf_menu') , 100 );

			/**
			 * Register Movies Post Type
			 */
			add_action( 'init', array( &$this, 'register_movies_post_type' ) );

			/**
			 * Register filter to show Movie List Automatically on frontpage
			 */
			add_filter( 'the_content', array( &$this, 'display_movie_on_frontpage' ) );
			
			/**
			 * Register Moxie Movie Meta Data
			 */
			$this->install_post_meta_data();

			/**
			 * Register ajax action for logged in and non logged in user 
			 */
			add_action( 'wp_ajax_get_movies_list', array( &$this, 'get_movies_list' ) );
			add_action( 'wp_ajax_nopriv_get_movies_list', array( &$this, 'get_movies_list' ) );
		}

		function moxie_class_setup()
		{
			global $wpdb;

			/**
			 * get Post object by slug 'movie-list'
			 */
			$post = get_page_by_path('movie-list','OBJECT','post');

			/**
			 * Check if post for Movie List already created
			 */
			if ($post == NULL )
			{
				$movie_list_post = array(
				  'post_title'    => 'Movie List',
				  'post_name'     => 'movie-list',
				  'post_content'  => '',
				  'post_status'   => 'publish',
				);
				$post_id = wp_insert_post($movie_list_post);
			} else {
				$post_id = $post->ID;
			}
			
			/**
			 * Sticky the post
			 */
			stick_post($post_id);
		}

		/**
		 * @param  $content
		 * @return $content
		 * Here we will change content of 'movie-list' post we create at plugin activation
		 * for displaying movie on frontpage
		 */
		function display_movie_on_frontpage($content)
		{
			global $wp_query;

			/**
			 * Lets check if current post being displayed on frontpage or not
			 * and its position on top
			 */
			if ( ( is_home() || is_front_page() ) & $wp_query->current_post == 0  )
			{
				/**
				 * Register and Enqueue css and js file
				 */
				wp_register_style( 
					'moxie-wp-test-css',
				    plugins_url('moxie-wp-test/assets/moxie-wp-test.css')
				);
				wp_enqueue_style('moxie-wp-test-css');

				wp_register_style( 
					'moxie-wp-test-slider-css',
				    plugins_url('moxie-wp-test/assets/jquery.bxslider.css')
				);
				wp_enqueue_style('moxie-wp-test-slider-css');

				wp_register_style( 
					'moxie-wp-test-loader-css',
				    plugins_url('moxie-wp-test/assets/loaders.min.css')
				);
				wp_enqueue_style('moxie-wp-test-loader-css');

				wp_register_script(
					'moxie-wp-test-js',
				    plugins_url('moxie-wp-test/assets/moxie-wp-test.js')
				);
				wp_enqueue_script('moxie-wp-test-js');

				wp_register_script(
					'moxie-wp-test-slider-js',
				    plugins_url('moxie-wp-test/assets/jquery.bxslider.min.js')
				);
				wp_enqueue_script('moxie-wp-test-slider-js');
				
				/**
				 * Wrapper for Movies list
				 */
				$moxie_movie_wrapper = 
					'<div id="moxie-wp-wrapper">
						<ul id="moxie-movie-slider" class="bxslider">
						</ul>
						<div id="moxie-no-movie">
						<p> Sorry ,  there are currently no movies to display yet :( </p>
						</div>
						<div id="moxie-movie-loading">
						<div class="loader-inner semi-circle-spin"><div></div></div>
						</div>
					</div>';

				/**
				 * Lets merge movie wrapper and $content from post
				 */
				$content = $moxie_movie_wrapper . $content ;
			}
			return $content;
		}

		/**
		 * Moxie Movie Meta Data and make sure ACF installed
		 */
		function install_post_meta_data()
		{
			if(function_exists("register_field_group"))
			{
				register_field_group(array (
					'id' => 'acf_movie-options',
					'title' => 'Movie Options',
					'fields' => array (
						array (
							'key' => 'field_56330ec63fc67',
							'label' => 'Poster URL',
							'name' => 'poster_url',
							'type' => 'text',
							'default_value' => '',
							'placeholder' => '',
							'prepend' => '',
							'append' => '',
							'formatting' => 'none',
							'maxlength' => '',
						),
						array (
							'key' => 'field_56330f203fc68',
							'label' => 'Movie Rating',
							'name' => 'movie_rating',
							'type' => 'number',
							'default_value' => '',
							'placeholder' => '',
							'prepend' => '',
							'append' => '',
							'min' => 0,
							'max' => 5,
							'step' => '',
						),
						array (
							'key' => 'field_56330f673fc69',
							'label' => 'Release Year',
							'name' => 'release_year',
							'type' => 'number',
							'default_value' => '',
							'placeholder' => '',
							'prepend' => '',
							'append' => '',
							'min' => '',
							'max' => '',
							'step' => '',
						),
						array (
							'key' => 'field_56330fba3fc6a',
							'label' => 'Short Description',
							'name' => 'short_description',
							'type' => 'wysiwyg',
							'instructions' => 'Short html description of the movie',
							'default_value' => '',
							'toolbar' => 'full',
							'media_upload' => 'yes',
						),
					),
					'location' => array (
						array (
							array (
								'param' => 'post_type',
								'operator' => '==',
								'value' => 'moxiemovie_post_type',
								'order_no' => 0,
								'group_no' => 0,
							),
						),
					),
					'options' => array (
						'position' => 'normal',
						'layout' => 'no_box',
						'hide_on_screen' => array (
							0 => 'permalink',
							1 => 'the_content',
							2 => 'excerpt',
							3 => 'discussion',
							4 => 'comments',
							5 => 'revisions',
							6 => 'slug',
							7 => 'author',
							8 => 'format',
							9 => 'featured_image',
							10 => 'categories',
							11 => 'tags',
							12 => 'send-trackbacks',
						),
					),
					'menu_order' => 0,
				));
			}
		}

		/**
		 * Remove ACF Menu from Admin
		 */
		function remove_acf_menu()
		{
			remove_menu_page( 'edit.php?post_type=acf' );
		}

		/**
		 * Install plugin depedencies ACF
		 */
		function install_required_plugins()
		{
			$plugins = array(
				array(
	            	'name' => 'Advanced Custom Fields',
	            	'slug' => 'advanced-custom-fields',
	            	'required' => true,
	            	'force_activation' => true,
        		)
        	);
        	$config = array(
        		'id'           => 'tgmpa-moxie',
        		'menu'         => 'tgmpa-install-plugins',
        		'has_notices'  => true,
        		'dismissable'  => false,
        		'is_automatic' => true,
        	);
        	tgmpa( $plugins, $config );
		}

		/**
		 * Register our Moxie Movie Post Type
		 */
		function register_movies_post_type() 
		{
			$labels = array(
				'name'                => _x( 'Movies', 'Post Type General Name', 'moxie_movies_test' ),
				'singular_name'       => _x( 'Movie', 'Post Type Singular Name', 'moxie_movies_test' ),
				'menu_name'           => __( 'Moxie Movie', 'moxie_movies_test' ),
				'name_admin_bar'      => __( 'Moxie Movie', 'moxie_movies_test' ),
				'parent_item_colon'   => __( 'Parent Movie:', 'moxie_movies_test' ),
				'all_items'           => __( 'All Movies', 'moxie_movies_test' ),
				'add_new_item'        => __( 'Add New Movie', 'moxie_movies_test' ),
				'add_new'             => __( 'Add New', 'moxie_movies_test' ),
				'new_item'            => __( 'New Movie', 'moxie_movies_test' ),
				'edit_item'           => __( 'Edit Movie', 'moxie_movies_test' ),
				'update_item'         => __( 'Update Movie', 'moxie_movies_test' ),
				'view_item'           => __( 'View Movie', 'moxie_movies_test' ),
				'search_items'        => __( 'Search Movie', 'moxie_movies_test' ),
				'not_found'           => __( 'Movie Not found', 'moxie_movies_test' ),
				'not_found_in_trash'  => __( 'No Movie found in Trash', 'moxie_movies_test' ),
			);
			$args = array(
				'label'               => __( 'Movie', 'moxie_movies_test' ),
				'description'         => __( 'A list of movies', 'moxie_movies_test' ),
				'labels'              => $labels,
				'supports'            => array( ),
				'taxonomies'          => array( ),
				'hierarchical'        => false,
				'public'              => true,
				'show_ui'             => true,
				'show_in_menu'        => true,
				'menu_position'       => 5,
				'show_in_admin_bar'   => true,
				'show_in_nav_menus'   => true,
				'can_export'          => true,
				'has_archive'         => true,		
				'exclude_from_search' => false,
				'publicly_queryable'  => true,
				'rewrite'             => false,
				'capability_type'     => 'page',
			);
			register_post_type( 'moxiemovie_post_type', $args );
		}

		/**
		 * @return JSON to frontend
		 * Fetch last 10 Moxie Movie Post Type.
		 */
		function get_movies_list()
		{
			/**
			 * [$movies_list description]
			 * Fetch last 10 Moxie Movie Post Type.
			 */
			$movies_list = get_posts( array( 'post_type' => 'moxiemovie_post_type' , 'posts_per_page' => '10' ) );
			$movies_to_send['data'] = array();

			/**
			 * Process $movie_list for JSON data.
			 */
			foreach ( $movies_list as $movie ){
				$movies_to_send['data'][] = array (
					'id' 				=> $movie->ID,
					'title' 			=> $movie->post_title,
					'poster_url' 		=> get_field('poster_url',$movie->ID),
					'rating'			=> (float) get_field('movie_rating',$movie->ID),
					'year'				=> (int) get_field('release_year',$movie->ID),
					'short_description'	=> get_field('short_description',$movie->ID)
				);
			}

			/**
			 * Send a JSON response back (Movies list) to an AJAX request, and die().
			 */
			wp_send_json($movies_to_send);
		}
		
	}

	/**
	 * Run the plugin.
	 */
	$moxie_wp_test = new MoxieWpTestPlugin();

	/**
	 * Register hook to run moxie_class_setup function when plugin is activated.
	 */
	register_activation_hook( __FILE__, array( &$moxie_wp_test, 'moxie_class_setup' ) );
}

